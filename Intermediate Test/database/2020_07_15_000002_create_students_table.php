<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('student_number')->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->unsignedBigInteger('classroom_id');
            $table->foreign('classroom_id')->references('id')->on('classrooms');
            $table->timestamps();
        });
    }

    // https://dev.to/snehalk/how-to-use-soft-delete-in-laravel-8-1bf9
    // add 'deleted_at' field/column to our database
    Schema::table('students', function (Blueprint $table) {
        $table->softDeletes();
    });
    
    Schema::table('students', function (Blueprint $table) {
        $table->dropSoftDeletes();
    });

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
