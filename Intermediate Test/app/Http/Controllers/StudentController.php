<?php

namespace App\Http\Controllers;
// https://www.laravelcode.com/post/laravel-8-crud-application-tutorial-with-example
// https://medium.com/@chrissoemma/laravel-5-8-delete-and-soft-delete-practical-examples-b9b71c0a97f
// https://www.nicesnippets.com/blog/laravel-exists-validation-example

// TODO: 5
/*  When fetching data for an individual student (e.g. GET /students/123),
    the response payload should include a nested object with information
    about the student's classroom. */

class StudentController extends Controller
{
    // show students list
    public function index() {
        $students = Student::latest();
        return $students->toJson();
    }

    // display the specified resource
    public function show(Request $request, Student $student) {
        $request->validate([
            'classroom_id' => 'required|exists:classrooms,id'
        ]);

        $classroom = DB::table('students')->where('classroom_id', $request->input('classroom_id'))->get();
        return view('student.show',compact('student'));
    }

    // show a form for creating a new resource
    public function create() {
        return view('student.create');
    }

    // store a newly created resource in storage
    public function store(Request $request) {
        $request->validate([
            'student_number' => 'required|unique:students,student_number|alpha_num', // https://laravel.com/docs/8.x/validation#rule-unique
            'first_name' => 'required',
            'last_name' => 'required',
            'classroom_id' => 'required|exists:classrooms,id' // https://laravel.com/docs/8.x/validation#rule-exists
        ]);

        Student::create($request->all());

        return response()->json(['res' => 'student record created sucessfuly']);
    }

    // show a form for editing a specific resource
    public function edit(Student $student) {
        return view('student.show',compact('student'));
    }

    // update the specified resource in storage
    public function update(Request $request, Student $student) {
        $request->validate([
            'student_number' => 'nullable', // https://laravel.com/docs/8.x/validation#a-note-on-optional-fields
            'first_name' => 'nullable',
            'last_name' => 'nullable'
        ]);
    
        $student->update($request->all());
    
        return response()->json(['res' => 'student record updated sucessfuly'])
    }

    // remove the specified resource from storage
    public function destroy(Student $student) {
        $student->delete();
    
        return response()->json(['res' => 'student record deleted sucessfuly']);
    }
}
