<?php

namespace App\Http\Controllers;

class StudentSearchController extends Controller
{
    // TODO: 5
        // In the JSON response payload, include the full_name for each student by combining the student's first and last name.
        // The closent I could find was joining two arrays using 'array_combine()' - https://www.w3schools.com/php/func_array_combine.asp
        // It's not really appticable in this case at we have a single array with both 'first_name' and 'last_name'

        /*
        $fname=array("Peter","Ben","Joe");
        $age=array("35","37","43");
        $c=array_combine($fname,$age);
        print_r($c); // Array ( [Peter] => 35 [Ben] => 37 [Joe] => 43 )
        */

    // https://laravel.com/docs/8.x/requests#determining-if-input-is-present
    // filter by first name
    if ($request->has('first_name')) {
        $students = DB::table('students')
                        ->select('first_name', 'last_name')
                        ->where('first_name', $request->input('first_name'))
                        ->get()
                        ->toArray();

        return json_encode($students); // https://laravel.io/forum/06-20-2016-how-to-pass-data-to-json-from-database
    }

    // filter by last name
    if ($request->has('last_name')) {
        $students = DB::table('students')
                        ->select('first_name', 'last_name')
                        ->where('last_name', $request->input('last_name'))
                        ->get()
                        ->toArray();

        return json_encode($students);
    }

    // filter by classroom id
    if ($request->has('classroom_id')) {
        $students = DB::table('students')
                        ->select('first_name', 'last_name')
                        ->where('classroom_id', $request->input('classroom_id'))
                        ->get()
                        ->toArray();

        return json_encode($students);
    }

    // filter by school id
    if ($request->has('school_id')) {
        // 1. retrieve list of classrooms by 'school_id'
        // 2. retrieve list of students by 'classroom_id'

        $classrooms_list = DB::table('classrooms')
                                ->where('school_id', $request->input('school_id'))
                                ->get();

        $students_list = [];

        // https://stackoverflow.com/questions/28256103/iterating-through-result-set-in-laravel-controller
        foreach($classrooms_list as $class) {
            $students = DB::table('students')
                            ->select('first_name', 'last_name')
                            ->where('classroom_id', $class->id)
                            ->get();

            array_push($students_list, $students);
        }

        return json_encode($students_list);
    }
}
