
/*
*
* Front End App to handle basic Student Object operations
*
* Requirements:
*
* 1. Write a class called Assignment, which has three instance variables.
*   title : a string
*   score : a double
*   description : a string
*
* 2. The Assignment class should validate and perform for the score input, based on the requirements below when initialized. Otherwise throw an error:
*       - Must be positive between 0 and 10
*       - If the decimal part is 0.6 or greater, round to next whole number.
*       - If the decimal part is 0.4 or lower, round to previous whole number.
*       - If the decimal part is 0.5, leave as it is. At the end returning scores should can only have 0 or .5 as the decimal part.
*
* 2. The Assignment class should have the following methods:
*   getQuickSummary: Print Assignment title and description in a readable format.
*   getAssignmentScore: Print score
*
* 3. Write a class called Student, which has three instance variables and constructor.
*   name : a string
*   last_name : a string
*   assignments : an array of items (of object type), with each item being an Assignment object that belong to the student.
*
* 4. The Student class should have the following methods:
*   getFullName: Print Student name plus last name
*   getAssignmentsScore: Print list of the User Assignments with the format: "$StudentName score was $AssignmentGrade for: $AssignmentName"
*   getFinalCourseScore:
*        Print message with final score and passing status based on the Assignments Array. It can be a simple calculation such as: (( $Score1 + $Score2 ...) / NumberOfScores).
*        Example output: "Steve's final score was 8. Steve passed the course."
*
*/


class Assignment {
    constructor(title, score, description) {
        this.title = title;

        // check the 'score' value
        if (score < 0 || score > 10) {
            throw new Error('Score is out of bounds.')
        }
        // find the value of decimal portion
        else if (Math.abs(score) - Math.floor(score) >= 0.6) { // round up
            this.score = Math.ceil(score);
        }
        else if (Math.abs(score) - Math.floor(score) <= 0.4) { // round down
            this.score = Math.floor(score);
        }
        else if (Math.abs(score) - Math.floor(score) == 0.5) { // don't round
            this.score = score;
        }
        else {
            this.score = this.score = score;
        }

        this.description = description;
    }

    getQuickSummary() {
        // assume an HTML tag with id='summary'
        // document.getElementById('summary').innerHTML = `Title: ${this.title}\n Description: ${this.description}`;
        console.log(`Title: ${this.title}\n Description: ${this.description}`);
    }

    getAssignmentScore() {
        // assume an HTML tag with id='score'
        // document.getElementById('score').innerHTML = `Score: ${this.score}`;
        console.log(`Score: ${this.score}`);
    }
}

class Student {
    constructor(name, last_name, assignments) {
        this.name = name;
        this.last_name = last_name;

        // check for array
        if (Array.isArray(assignments)) {
            assignments.forEach((assignment) => {
                // check for class instance
                if (!assignment instanceof Assignment) {
                    throw new Error('An element is not an instanse of Class Assignment');
                }
            });

            this.assignments = assignments;
        }
        else {
            throw new Error('Assignments should be of type Array');
        }
    }

    getFullName() {
        console.log(`${this.name} ${this.last_name}`)
    }

    getAssignmentsScore() {
        this.assignments.forEach((assignment) => {
            console.log(`${this.name} score was ${assignment.score} for: ${assignment.title}`);
        });
    }

    getFinalCourseScore() {
        let scores = this.assignments.map(asgmt => asgmt.score);
        const reducer = (previousValue, currentValue) => previousValue + currentValue;
        let finalScore = scores.reduce(reducer) / scores.length // sum all values in the array and devide by array length

        console.log(`${this.name}'s final score was ${finalScore}. ${this.name} passed the course.`)
    }
}